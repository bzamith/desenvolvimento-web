# Bolão da Copa - NodeJs

## Projeto

Adaptações em cima do sistema de Bolão da Copa, desenvolvido pelo professor Dr. Daniel Lucredio na disciplina de 
Desenvolvimento de Software para Web, primeiro semestre de 2018 - UFSCar. Os roteiros foram criados também pelo professor Dr. Daniel Lucredio, referente ao projeto Bolão da Copa original. 

## Adaptações

Implementei a funcionalidade de limitar o número de palpites por usuário, sendo o máximo determinado igual a 2. 
Para tal, foi criado/implementado:

### Back-end
- [x] Classe Bean Contagem;
- [x] Função listarQntdPalpitesPorEmail() na Classe DAO PalpiteDAO;
- [x] Função getContagem() na Classe ServicosPalpite;
  
### Front-end
- [x] Adaptações e verificações na função handleEmailChanged() do PalpiteForm.js
- [x] Criação da função (estado) contagemExcedida() no NovoPalpiteMaquinaEstados.js
  
## Execução

No projeto não estão inclusos os módulos necessários, para instalá-los, executar no diretório bolao-da-copa-v2-front-end:

```shell
  sudo npm install react-router-dom -S
  sudo npm install classnames -S
  sudo npm install font-awesome -S
  sudo npm install primereact -S

```
